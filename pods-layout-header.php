<?php

namespace PODS\Layout;


// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}


class Header
{
   /**
    * Constructor
    *
    * @return void
    */
    public function __construct()
    {
        add_action('acf/input/admin_head', [$this, 'acfStyles'], 20);
        add_action('init', [$this, 'defineFields'], 20);
    }


    /**
     * Add styles to ACF.
     *
     * @return void
     */
    public function acfStyles()
    {
        $styles = '
            <style type="text/css">
                ::-webkit-input-placeholder { color: #d0d0d0 !important; }
                ::-moz-placeholder { color: #d0d0d0 !important; }

                .acf-flexible-content .layout { border: 2px solid #454545; }

                .acf-flexible-content .layout .acf-fc-layout-handle {
                    background: #454545;
                    border-bottom: none;
                    color: #fff;
                }

                .acf-field-message {
                    background: #e2e2df;
                    text-transform: uppercase;
                    color: #333;
                    border: none !important;
                    border-top: 4px solid #bec1b5 !important;
                }
                .acf-field-message p,
                .acf-field-message .acf-label { margin: 0; }
                .acf-fields > .acf-field:first-child {
                    margin-top: 0 !important;
                }

                .toggler {
                    padding: 0 !important;
                    margin-top: -45px !important;
                    margin-right: 10px !important;
                    float: right;
                    position: relative;
                    clear: both;
                    border: none !important;
                    font-size: 11px;
                    color: #999;
                }
                .toggler .acf-label { display: none; }

                .acf-repeater .acf-row-handle {
                    vertical-align: top !important;
                    font-size: 20px;
                }

                .acf-repeater tr td {
                    border-bottom: 10px solid #f9f9f9 !important;
                }
            </style>
        ';

        $script = '
            <script type="text/javascript">
                (function($){
                })(jQuery);
            </script>
        ';

        echo $styles;
    }


    /**
     * create page header
     *
     * @return void
    */

    public static function display($title = '', $pretitle = '', $subtitle = '', $nav = false)
    {
        if (is_single()) {
            global $post;
            $post_object = get_post($post->ID);
            setup_postdata($post_object);
        }

        $title = (! $title ? get_the_title() : $title);
        $layout = ''; // Set layout
        $option = (is_home() ? "option" : ''); // Set Option
        $nav = ($nav ? '
            <nav class="nav nav--tabbed">
                <ul class="nav__list container">'
                    .\PODS\Layout\Navigation::display('top', 1).
                '</ul>
            </nav>'
        : '');

        // Set Flex Layout
        if (have_rows('flex_header', $option) && ! is_archive()) {
            while (have_rows('flex_header', $option)) {
                the_row();

                $pretitle = (get_sub_field('text_pretitle') ? get_sub_field('text_pretitle') : $pretitle);
                $title = (get_sub_field('text_title') ? get_sub_field('text_title') : $title);
                $subtitle = (get_sub_field('text_subtitle') ? get_sub_field('text_subtitle') : $subtitle);

                // Row Header
                $row_pretitle = ($pretitle ? '<div class="layout__header-pretitle">'.$pretitle.'</div>' : '');
                $row_title = ($title ? '<h1 class="layout__header-title">'.$title.'</h1>' : '');
                $row_subtitle = ($subtitle ? '<div class="layout__header-subtitle">'.get_sub_field('text_subtitle').'</div>' : '');

                $row_header = ($row_pretitle || $row_title || $row_subtitle ? $row_pretitle.$row_title.$row_subtitle : '');

                // Row Settings
                $row_inverse = (get_sub_field('boolean_inverse') ? 'text-inverse' : '');
                $row_id = (get_sub_field('text_id') ? 'id="'.get_sub_field('text_id').'"' : '');
                $row_class = (get_sub_field('text_class') ? ''.get_sub_field('text_class') : '');
                $row_container = (get_sub_field('select_container') ? 'class="layout__container '.get_sub_field('select_container').'"' : '');

                // Row Appearance
                $row_background_image = (get_sub_field('image_background') ? 'background-image:url('.get_sub_field('image_background')['url'].');' : '');
                $row_header_image = ($row_background_image ? 'layout__header--image' : '');
                $row_background = ($row_background_image ? 'style="'.$row_background_image.'"' : '');

                // Row Structure
                $layout .= '
                    <header '.$row_id.' class="layout__header '.$row_class.' '.$row_header_image.'" '.$row_background.'>
                        <div class="banner__inner">
                            <div '.$row_container.'>'
                                . $row_header .
                            '</div>
                        </div>'
                        . $nav . '
                    </header>
                ';
            }
        } else {
            $layout = '
                <header class="layout__header">
                  <div class="banner__inner">
                    <div class="layout__container">'
                        . ($pretitle ? '<div class="layout__header-pretitle">' . $pretitle .'</div>' : '') .
                        '<h1 class="layout__header-title">'. $title .'</h1>'
                        . ($subtitle ? '<div class="layout__header-subtitle">' . $subtitle .'</div>' : '')
                    .'</div>
                  </div>'
                  . $nav .
                '</header>
            ';
        }
        return $layout;
    }


    /**
     * Define fields for page layout.
     *
     * @return void
     */
    public function defineFields()
    {
        // Loop through potential locations
        $locations = [];

        // Add Options Homepage
        $locations[] = array (
            array (
                'param' => 'options_page',
                'operator' => '==',
                'value' => 'acf-options-homepage'
            )
        );
        foreach (get_post_types(['public' => true]) as $post_type) {
            $locations[] = array (
                array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => $post_type
                )
            );
        }

        // Flex Layout
        if (function_exists('acf_add_local_field_group')) {
            acf_add_local_field_group(array (
                'key' => 'group_5798b40a50cba',
                'title' => '[layout] Header',
                'fields' => array (
                    array (
                        'key' => 'field_5798b40a5637d',
                        'label' => 'Header Layout',
                        'name' => 'flex_header',
                        'type' => 'flexible_content',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'button_label' => 'Add Page Header Layout',
                        'min' => '',
                        'max' => 1,
                        'layouts' => array (
                            array (
                                'key' => '578e471d8efe9',
                                'name' => 'header',
                                'label' => 'Header',
                                'display' => 'block',
                                'sub_fields' => array (
                                    array (
                                        'key' => 'field_5798b40a5858f',
                                        'label' => 'Row Settings',
                                        'name' => 'row_settings',
                                        'type' => 'message',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => 0,
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'message' => '',
                                        'new_lines' => 'wpautop',
                                        'esc_html' => 0,
                                    ),
                                    array (
                                        'key' => 'field_5798b40a5859b',
                                        'label' => 'Toggle Settings',
                                        'name' => 'toggle_settings',
                                        'type' => 'true_false',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => 0,
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => 'toggler',
                                            'id' => '',
                                        ),
                                        'message' => 'Toggle Row Settings',
                                        'default_value' => 1,
                                    ),
                                    array (
                                        'key' => 'field_5798b40a585b8',
                                        'label' => 'Container',
                                        'name' => 'select_container',
                                        'type' => 'select',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_5798b40a5859b',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 20,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'choices' => array (
                                            'layout__container--basic' => 'Basic',
                                            'layout__container--fluid' => 'Fluid',
                                            'layout__container--none' => 'None',
                                        ),
                                        'default_value' => array (
                                        ),
                                        'allow_null' => 0,
                                        'multiple' => 0,
                                        'ui' => 0,
                                        'ajax' => 0,
                                        'placeholder' => '',
                                        'disabled' => 0,
                                        'readonly' => 0,
                                        'return_format' => 'value',
                                    ),
                                    array (
                                        'key' => 'field_5798b40a585e5',
                                        'label' => 'ID',
                                        'name' => 'text_id',
                                        'type' => 'text',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_5798b40a5859b',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 20,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'default_value' => '',
                                        'placeholder' => 'lowercase-class',
                                        'prepend' => '',
                                        'append' => '',
                                        'maxlength' => '',
                                        'readonly' => 0,
                                        'disabled' => 0,
                                    ),
                                    array (
                                        'key' => 'field_5798b40a58604',
                                        'label' => 'Class',
                                        'name' => 'text_class',
                                        'type' => 'text',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_5798b40a5859b',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 20,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'default_value' => '',
                                        'placeholder' => 'lowercase-class',
                                        'prepend' => '',
                                        'append' => '',
                                        'maxlength' => '',
                                        'readonly' => 0,
                                        'disabled' => 0,
                                    ),
                                    array (
                                        'key' => 'field_5798b40a58677',
                                        'label' => 'Background Image',
                                        'name' => 'image_background',
                                        'type' => 'image',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_5798b40a5859b',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 20,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'return_format' => 'array',
                                        'preview_size' => 'thumbnail',
                                        'library' => 'all',
                                        'min_width' => '',
                                        'min_height' => '',
                                        'min_size' => '',
                                        'max_width' => '',
                                        'max_height' => '',
                                        'max_size' => '',
                                        'mime_types' => '',
                                    ),
                                    array (
                                        'key' => 'field_5798b40a58536',
                                        'label' => 'Row Header',
                                        'name' => '',
                                        'type' => 'message',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => 0,
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'message' => '',
                                        'new_lines' => 'wpautop',
                                        'esc_html' => 0,
                                    ),
                                    array (
                                        'key' => 'field_5798b40a58546',
                                        'label' => 'Toggle Header',
                                        'name' => 'toggle_header',
                                        'type' => 'true_false',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => 0,
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => 'toggler',
                                            'id' => '',
                                        ),
                                        'message' => 'Toggle Row Header',
                                        'default_value' => 0,
                                    ),
                                    array (
                                        'key' => 'field_5798b40a58565',
                                        'label' => 'Pre-Title',
                                        'name' => 'text_pretitle',
                                        'type' => 'text',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_5798b40a58546',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 25,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'default_value' => '',
                                        'placeholder' => 'If necessary',
                                        'prepend' => '',
                                        'append' => '',
                                        'maxlength' => '',
                                        'readonly' => 0,
                                        'disabled' => 0,
                                    ),
                                    array (
                                        'key' => 'field_5798b40a58578',
                                        'label' => 'Title',
                                        'name' => 'text_title',
                                        'type' => 'text',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_5798b40a58546',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 35,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'default_value' => '',
                                        'placeholder' => 'If necessary',
                                        'prepend' => '',
                                        'append' => '',
                                        'maxlength' => '',
                                        'readonly' => 0,
                                        'disabled' => 0,
                                    ),
                                    array (
                                        'key' => 'field_5798b40a58584',
                                        'label' => 'Sub Title',
                                        'name' => 'text_subtitle',
                                        'type' => 'textarea',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_5798b40a58546',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 40,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'default_value' => '',
                                        'placeholder' => '',
                                        'maxlength' => '',
                                        'rows' => 1,
                                        'new_lines' => 'wpautop',
                                        'readonly' => 0,
                                        'disabled' => 0,
                                    ),
                                ),
                                'min' => '',
                                'max' => '',
                            ),
                        ),
                    ),
                ),
                'location' => array (
                    array (
                        array (
                            'param' => 'post_type',
                            'operator' => '==',
                            'value' => 'page',
                        ),
                    ),
                    array (
                        array (
                            'param' => 'post_type',
                            'operator' => '==',
                            'value' => 'post',
                        ),
                    ),
                ),
                'menu_order' => 0,
                'position' => 'acf_after_title',
                'style' => 'seamless',
                'label_placement' => 'top',
                'instruction_placement' => 'label',
                'hide_on_screen' => '',
                'active' => 1,
                'description' => '',
            ));
        }
    }
}

new Header;
