<?php

namespace PODS\Layout;


// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}


class Page
{
   /**
    * Constructor
    *
    * @return void
    */
    public function __construct()
    {
        add_action('acf/input/admin_head', [$this, 'acfStyles'], 20);
        add_filter('the_content', [$this, 'display'], 10, 2);
        add_action('init', [$this, 'defineFields'], 20);
    }


    /**
     * Add styles to ACF.
     *
     * @return void
     */
    public function acfStyles()
    {
        $styles = '
            <style type="text/css">
                ::-webkit-input-placeholder { color: #d0d0d0 !important; }
                ::-moz-placeholder { color: #d0d0d0 !important; }

                .acf-flexible-content .layout { border: 2px solid #454545; }

                .acf-flexible-content .layout .acf-fc-layout-handle {
                    background: #454545;
                    border-bottom: none;
                    color: #fff;
                }

                .acf-field-message {
                    background: #e2e2df;
                    text-transform: uppercase;
                    color: #333;
                    border: none !important;
                    border-top: 4px solid #bec1b5 !important;
                }
                .acf-field-message p,
                .acf-field-message .acf-label { margin: 0; }
                .acf-fields > .acf-field:first-child {
                    margin-top: 0 !important;
                }

                .toggler {
                    padding: 0 !important;
                    margin-top: -45px !important;
                    margin-right: 10px !important;
                    float: right;
                    position: relative;
                    clear: both;
                    border: none !important;
                    font-size: 11px;
                    color: #999;
                }
                .toggler .acf-label { display: none; }

                .acf-repeater .acf-row-handle {
                    vertical-align: top !important;
                    font-size: 20px;
                }

                .acf-repeater tr td {
                    border-bottom: 10px solid #f9f9f9 !important;
                }
            </style>
        ';

        $script = '
            <script type="text/javascript">
                (function($){
                })(jQuery);
            </script>
        ';

        echo $styles;
    }


    /**
     * Display the layout
     * @param  string $content  the WP content
     * @return string           concatenated string of content & layout
     */
    public static function display($content = '')
    {
        // Set layout
        $layout = '';
        $option = (is_home() ? "option" : '');
        $i_row = 0;

        if (! is_home()) {
            global $post;
            $post_object = get_post($post->ID);
            setup_postdata($post_object);
        }

        // Set Flex Layout
        if (have_rows('flex_page', $option)) {
            while (have_rows('flex_page', $option)) {
                the_row();
                $i_row++;

                // Row Header
                $row_pretitle = (get_sub_field('text_pretitle') ? '
                    <div class="layout__section-pretitle">'
                        .get_sub_field('text_pretitle').
                    '</div>': ''
                );
                $row_title = (get_sub_field('text_title') ? '
                    <h1 class="layout__section-title">'
                        .get_sub_field('text_title').
                    '</h1>' : ''
                );
                $row_subtitle = (get_sub_field('text_subtitle') ? '
                    <div class="layout__section-subtitle">'
                        .get_sub_field('text_subtitle').
                    '</div>' : ''
                );
                $row_header = ($row_pretitle || $row_title || $row_subtitle ? '
                    <header class="layout__section-header">
                        '.$row_pretitle.'
                        '.$row_title.'
                        '.$row_subtitle.'
                    </header>' : ''
                );

                // Row Settings
                $row_banner = (get_sub_field('select_banner') ? 'banner '. implode(' ', get_sub_field('select_banner')) : '');
                $row_id = (get_sub_field('text_id') ? 'id="'.get_sub_field('text_id').'"' : '');
                $row_class = (get_sub_field('text_class') ? get_sub_field('text_class') : '');
                $row_container = (get_sub_field('select_container') ? 'class="layout__container '.get_sub_field('select_container').'"' : 'class="layout__container"');

                // Row Appearance
                $row_background_color = (get_sub_field('color_background') ? 'background-color:'.get_sub_field('color_background').';' : '');
                $row_background_image = (get_sub_field('image_background') ? 'background-image:url('.get_sub_field('image_background')['url'].');' : '');
                $row_section_image = ($row_background_image ? 'layout__section--image' : '');
                $row_background = ($row_background_color || $row_background_image ? 'style="'.$row_background_color.$row_background_image.'"' : '');

                // Row Structure
                $section_open = '
                    <section '.$row_id.' class="layout__section '. $row_section_image .' '. $row_class .' '. $row_banner .'" '.$row_background.'>
                        '. ($row_banner ? '<div class="banner__inner">' : '') .'
                        <div '.$row_container.'>'
                            .$row_header;
                $section_content = '';
                $section_close = '
                        '. ($row_banner ? '</div>' : '') .'
                        </div>
                    </section>
                ';

                // Layout Grid
                if (get_row_layout() == 'grid') {
                    // Row Columns
                    $row_columns = '';
                    if (have_rows('repeater_columns')) {
                        while (have_rows('repeater_columns')) {
                            the_row();
                            $column_size = get_sub_field('select_column_size');
                            $column_size_class = str_replace(['col-offset','col-push','col-pull'], ['offset','push','pull'], ($column_size ? 'col-'. implode(' col-', $column_size) : ''));
                            $column_content = get_sub_field('wysiwyg_content');
                            $column_class = get_sub_field('text_column_class');

                            $row_class = $column_size_class .' '. $column_class;
                            $row_columns .= '
                                <div class="'.trim($row_class).'">'
                                    .$column_content.
                                '</div>
                            ';
                        }
                    }

                    $section_content .= '
                        <div class="layout__row">
                            '.$row_columns.'
                        </div>
                    ';
                }

                // Layout Tab
                if (get_row_layout() == 'tab') {
                    if (have_rows('repeater_tabs')) {
                        $row_tabs = '';
                        $row_tabs_content = '';
                        $i = 0;

                        while (have_rows('repeater_tabs')) {
                            the_row();
                            $i++;

                            $row_tabs .= '
                                <li class="layout__tabs__tab nav__item nav-item">
                                    <a class="nav__link '.($i == 1 ? 'active' : '').'"
                                        data-toggle="tab"
                                        href="#tab-'.$i.'"
                                        role="tab">'
                                        .get_sub_field('text_title').
                                    '</a>
                                </li>
                            ';
                            $row_tabs_content .= '
                                <div class="layout__tabs__pane tab-pane '.($i == 1 ? 'active' : '').'"
                                    id="tab-'.$i.'"
                                    role="tabpanel"> '
                                    .get_sub_field('wysiwyg_content').
                                '</div>
                            ';
                        }
                        reset_rows();
                    }


                    $section_content .= '
                        <div class="layout__tabs nav nav--tabbed nav--tabbed--border">
                            <ul class="layout__tabs__nav nav__list" role="tablist">'
                                .$row_tabs.
                            '</ul>
                            <div class="layout__tabs__content tab-content">'
                                .$row_tabs_content.
                            '</div>
                        </div>
                    ';
                }

                // Layout Accordion
                if (get_row_layout() == 'accordion') {
                    if (have_rows('repeater_accordion')) {
                        $row_accordion_content = '';
                        $i = 0;

                        while (have_rows('repeater_accordion')) {
                            the_row();
                            $i++;

                            $row_accordion_content .= '
                                <div class="card">
                                    <div class="layout__accordion__title accordion__trigger collapsed"
                                        data-toggle="collapse"
                                        data-parent="#accordion-'.$i_row.'"
                                        data-target="#accordion-'.$i_row.'-collapse-'.$i.'"
                                        aria-expanded="false"
                                        aria-controls="accordion-'.$i_row.'-collapse-'.$i.'"
                                        aria-label="accordion-'.$i_row.'-collapse-'.$i.'"
                                        role="tab">'
                                            .get_sub_field('text_title').
                                    '</div>
                                    <div class="layout__accordion__body accordion__content collapse"
                                        id="accordion-'.$i_row.'-collapse-'.$i.'"
                                        aria-labelledby="accordion-'.$i_row.'-collapse-'.$i.'"
                                        role="tabpanel">
                                        <div class="accordion__inner">'
                                            .get_sub_field('wysiwyg_content').
                                        '</div>
                                    </div>
                                </div>
                            ';
                        }
                    }

                    $section_content .= '
                        <div class="layout__accordion accordion"
                            id="accordion-'.$i_row.'"
                            aria-multiselectable="false"
                            role="tablist">'
                            .$row_accordion_content.
                        '</div>
                    ';
                }

                $layout .= $section_open . $section_content . $section_close;
            }
        }

        $content = ($content ? '
            <div class="layout__section layout__section-content">
                <div class="layout__container">'
                    . $content .
                '</div>
            </div>
        ' : '');
        return $content . $layout;
    }


    /**
     * Define fields for page layout.
     *
     * @return void
     */
    public function defineFields()
    {
        // Add Options Homepage
        $locations = array (
            array (
                array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'page',
                ),
            ),
            array (
                array (
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'acf-options-homepage'
                )
            )
        );

        // Flex Layout
        if ( function_exists('acf_add_local_field_group') ) {
            acf_add_local_field_group(array (
                'key' => 'group_578e46f842c20',
                'title' => '[layout] Page',
                'fields' => array (
                    array (
                        'key' => 'field_578e4705f49b7',
                        'label' => 'Page Layout',
                        'name' => 'flex_page',
                        'type' => 'flexible_content',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'button_label' => 'Add Page Layout',
                        'min' => '',
                        'max' => '',
                        'layouts' => array (
                            array (
                                'key' => '578e471d8efe9',
                                'name' => 'grid',
                                'label' => 'Grid Row',
                                'display' => 'block',
                                'sub_fields' => array (
                                    array (
                                        'key' => 'field_578f9e89fe5b7',
                                        'label' => 'Row Settings',
                                        'name' => '',
                                        'type' => 'message',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => 0,
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'message' => '',
                                        'new_lines' => 'wpautop',
                                        'esc_html' => 0,
                                    ),
                                    array (
                                        'key' => 'field_578fe58f85f86',
                                        'label' => 'Toggle Settings',
                                        'name' => 'toggle_settings',
                                        'type' => 'true_false',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => 0,
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => 'toggler',
                                            'id' => '',
                                        ),
                                        'message' => 'Toggle Row Settings',
                                        'default_value' => 0,
                                    ),
                                    array (
                                        'key' => 'field_578fed54cab63',
                                        'label' => 'Container',
                                        'name' => 'select_container',
                                        'type' => 'select',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_578fe58f85f86',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 20,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'choices' => array (
                                            'layout__container--basic' => 'Basic',
                                            'layout__container--fluid' => 'Fluid',
                                            'layout__container--none' => 'None',
                                        ),
                                        'default_value' => array (
                                        ),
                                        'allow_null' => 0,
                                        'multiple' => 0,
                                        'ui' => 0,
                                        'ajax' => 0,
                                        'placeholder' => '',
                                        'disabled' => 0,
                                        'readonly' => 0,
                                        'return_format' => 'value',
                                    ),
                                    array (
                                        'key' => 'field_578f94356a9f1',
                                        'label' => 'ID',
                                        'name' => 'text_id',
                                        'type' => 'text',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_578fe58f85f86',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 20,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'default_value' => '',
                                        'placeholder' => 'lowercase-id',
                                        'prepend' => '',
                                        'append' => '',
                                        'maxlength' => '',
                                        'readonly' => 0,
                                        'disabled' => 0,
                                    ),
                                    array (
                                        'key' => 'field_578e475cf49b9',
                                        'label' => 'Class',
                                        'name' => 'text_class',
                                        'type' => 'text',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_578fe58f85f86',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 20,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'default_value' => '',
                                        'placeholder' => 'lowercase-class',
                                        'prepend' => '',
                                        'append' => '',
                                        'maxlength' => '',
                                        'readonly' => 0,
                                        'disabled' => 0,
                                    ),
                                    array (
                                        'key' => 'field_578f81452c2dd',
                                        'label' => 'Banner',
                                        'name' => 'select_banner',
                                        'type' => 'select',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_578fe58f85f86',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => '20',
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'choices' => array (
                                            'banner--light' => 'Light',
                                            'banner--dark' => 'Dark',
                                            'banner--primary' => 'Primary',
                                            'banner--secondary' => 'Secondary',
                                            'banner--tertiary' => 'Tertiary',
                                            'banner--xs' => 'Extra Small',
                                            'banner--sm' => 'Small',
                                            'banner--lg' => 'Large',
                                            'banner--xl' => 'Extra Large',
                                            'banner--top' => 'Align Top',
                                            'banner--middle' => 'Align Middle',
                                            'banner--bottom' => 'Align Bottom',
                                        ),
                                        'default_value' => array (
                                        ),
                                        'allow_null' => 1,
                                        'multiple' => 1,
                                        'ui' => 1,
                                        'ajax' => 0,
                                        'return_format' => 'value',
                                        'placeholder' => '',
                                    ),
                                    array (
                                        'key' => 'field_578f81d42c2e0',
                                        'label' => 'Background Image',
                                        'name' => 'image_background',
                                        'type' => 'image',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_578fe58f85f86',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 20,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'return_format' => 'array',
                                        'preview_size' => 'thumbnail',
                                        'library' => 'all',
                                        'min_width' => '',
                                        'min_height' => '',
                                        'min_size' => '',
                                        'max_width' => '',
                                        'max_height' => '',
                                        'max_size' => '',
                                        'mime_types' => '',
                                    ),
                                    array (
                                        'key' => 'field_578e503bb932c',
                                        'label' => 'Row Header',
                                        'name' => '',
                                        'type' => 'message',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => 0,
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'message' => '',
                                        'new_lines' => 'wpautop',
                                        'esc_html' => 0,
                                    ),
                                    array (
                                        'key' => 'field_578fa2281d5d8',
                                        'label' => 'Toggle Header',
                                        'name' => 'toggle_header',
                                        'type' => 'true_false',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => 0,
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => 'toggler',
                                            'id' => '',
                                        ),
                                        'message' => 'Toggle Row Header',
                                        'default_value' => 0,
                                    ),
                                    array (
                                        'key' => 'field_578f9cc7bdab9',
                                        'label' => 'Pre-Title',
                                        'name' => 'text_pretitle',
                                        'type' => 'text',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_578fa2281d5d8',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 25,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'default_value' => '',
                                        'placeholder' => 'If necessary',
                                        'prepend' => '',
                                        'append' => '',
                                        'maxlength' => '',
                                        'readonly' => 0,
                                        'disabled' => 0,
                                    ),
                                    array (
                                        'key' => 'field_578e4fd2b932a',
                                        'label' => 'Title',
                                        'name' => 'text_title',
                                        'type' => 'text',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_578fa2281d5d8',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 35,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'default_value' => '',
                                        'placeholder' => 'If necessary',
                                        'prepend' => '',
                                        'append' => '',
                                        'maxlength' => '',
                                        'readonly' => 0,
                                        'disabled' => 0,
                                    ),
                                    array (
                                        'key' => 'field_578f9ce7bdaba',
                                        'label' => 'Sub Title',
                                        'name' => 'text_subtitle',
                                        'type' => 'textarea',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_578fa2281d5d8',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 40,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'default_value' => '',
                                        'placeholder' => 'If necessary',
                                        'maxlength' => '',
                                        'rows' => 1,
                                        'new_lines' => 'wpautop',
                                        'readonly' => 0,
                                        'disabled' => 0,
                                    ),
                                    array (
                                        'key' => 'field_578e504bb932d',
                                        'label' => 'Row Content',
                                        'name' => '',
                                        'type' => 'message',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => 0,
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'message' => '',
                                        'new_lines' => 'wpautop',
                                        'esc_html' => 0,
                                    ),
                                    array (
                                        'key' => 'field_578fe618e188c',
                                        'label' => 'Toggle Row Content',
                                        'name' => 'toggle_content',
                                        'type' => 'true_false',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => 0,
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => 'toggler',
                                            'id' => '',
                                        ),
                                        'message' => 'Toggle Row Content',
                                        'default_value' => 1,
                                    ),
                                    array (
                                        'key' => 'field_578e4da0b9327',
                                        'label' => 'Columns',
                                        'name' => 'repeater_columns',
                                        'type' => 'repeater',
                                        'instructions' => 'Add content to the grid row by creating columns, based on Bootstrap\'s 12 column grid system.',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_578fe618e188c',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'collapsed' => 'field_578e4806f49bc',
                                        'min' => 1,
                                        'max' => 12,
                                        'layout' => 'table',
                                        'button_label' => 'Add Column',
                                        'sub_fields' => array (
                                            array (
                                                'key' => 'field_578e4806f49bc',
                                                'label' => 'Column Size',
                                                'name' => 'select_column_size',
                                                'type' => 'select',
                                                'instructions' => '',
                                                'required' => 0,
                                                'conditional_logic' => 0,
                                                'wrapper' => array (
                                                    'width' => '20',
                                                    'class' => '',
                                                    'id' => '',
                                                ),
                                                'choices' => array (
                                                    '1@xs' => '1@xs',
                                                    '1@sm' => '1@sm',
                                                    '1@md' => '1@md',
                                                    '1@lg' => '1@lg',
                                                    '2@xs' => '2@xs',
                                                    '2@sm' => '2@sm',
                                                    '2@md' => '2@md',
                                                    '2@lg' => '2@lg',
                                                    '3@xs' => '3@xs',
                                                    '3@sm' => '3@sm',
                                                    '3@md' => '3@md',
                                                    '3@lg' => '3@lg',
                                                    '4@xs' => '4@xs',
                                                    '4@sm' => '4@sm',
                                                    '4@md' => '4@md',
                                                    '4@lg' => '4@lg',
                                                    '5@xs' => '5@xs',
                                                    '5@sm' => '5@sm',
                                                    '5@md' => '5@md',
                                                    '5@lg' => '5@lg',
                                                    '6@xs' => '6@xs',
                                                    '6@sm' => '6@sm',
                                                    '6@md' => '6@md',
                                                    '6@lg' => '6@lg',
                                                    '7@xs' => '7@xs',
                                                    '7@sm' => '7@sm',
                                                    '7@md' => '7@md',
                                                    '7@lg' => '7@lg',
                                                    '8@xs' => '8@xs',
                                                    '8@sm' => '8@sm',
                                                    '8@md' => '8@md',
                                                    '8@lg' => '8@lg',
                                                    '9@xs' => '9@xs',
                                                    '9@sm' => '9@sm',
                                                    '9@md' => '9@md',
                                                    '9@lg' => '9@lg',
                                                    '10@xs' => '10@xs',
                                                    '10@sm' => '10@sm',
                                                    '10@md' => '10@md',
                                                    '10@lg' => '10@lg',
                                                    '11@xs' => '11@xs',
                                                    '11@sm' => '11@sm',
                                                    '11@md' => '11@md',
                                                    '11@lg' => '11@lg',
                                                    '12@xs' => '12@xs',
                                                    '12@sm' => '12@sm',
                                                    '12@md' => '12@md',
                                                    '12@lg' => '12@lg',
                                                    'pull-0@xs' => 'pull-0@xs',
                                                    'pull-1@xs' => 'pull-1@xs',
                                                    'pull-2@xs' => 'pull-2@xs',
                                                    'pull-3@xs' => 'pull-3@xs',
                                                    'pull-4@xs' => 'pull-4@xs',
                                                    'pull-5@xs' => 'pull-5@xs',
                                                    'pull-6@xs' => 'pull-6@xs',
                                                    'pull-7@xs' => 'pull-7@xs',
                                                    'pull-8@xs' => 'pull-8@xs',
                                                    'pull-9@xs' => 'pull-9@xs',
                                                    'pull-10@xs' => 'pull-10@xs',
                                                    'pull-11@xs' => 'pull-11@xs',
                                                    'pull-12@xs' => 'pull-12@xs',
                                                    'pull-0@sm' => 'pull-0@sm',
                                                    'pull-1@sm' => 'pull-1@sm',
                                                    'pull-2@sm' => 'pull-2@sm',
                                                    'pull-3@sm' => 'pull-3@sm',
                                                    'pull-4@sm' => 'pull-4@sm',
                                                    'pull-5@sm' => 'pull-5@sm',
                                                    'pull-6@sm' => 'pull-6@sm',
                                                    'pull-7@sm' => 'pull-7@sm',
                                                    'pull-8@sm' => 'pull-8@sm',
                                                    'pull-9@sm' => 'pull-9@sm',
                                                    'pull-10@sm' => 'pull-10@sm',
                                                    'pull-11@sm' => 'pull-11@sm',
                                                    'pull-12@sm' => 'pull-12@sm',
                                                    'pull-0@md' => 'pull-0@md',
                                                    'pull-1@md' => 'pull-1@md',
                                                    'pull-2@md' => 'pull-2@md',
                                                    'pull-3@md' => 'pull-3@md',
                                                    'pull-4@md' => 'pull-4@md',
                                                    'pull-5@md' => 'pull-5@md',
                                                    'pull-6@md' => 'pull-6@md',
                                                    'pull-7@md' => 'pull-7@md',
                                                    'pull-8@md' => 'pull-8@md',
                                                    'pull-9@md' => 'pull-9@md',
                                                    'pull-10@md' => 'pull-10@md',
                                                    'pull-11@md' => 'pull-11@md',
                                                    'pull-12@md' => 'pull-12@md',
                                                    'pull-0@lg' => 'pull-0@lg',
                                                    'pull-1@lg' => 'pull-1@lg',
                                                    'pull-2@lg' => 'pull-2@lg',
                                                    'pull-3@lg' => 'pull-3@lg',
                                                    'pull-4@lg' => 'pull-4@lg',
                                                    'pull-5@lg' => 'pull-5@lg',
                                                    'pull-6@lg' => 'pull-6@lg',
                                                    'pull-7@lg' => 'pull-7@lg',
                                                    'pull-8@lg' => 'pull-8@lg',
                                                    'pull-9@lg' => 'pull-9@lg',
                                                    'pull-10@lg' => 'pull-10@lg',
                                                    'pull-11@lg' => 'pull-11@lg',
                                                    'pull-12@lg' => 'pull-12@lg',
                                                    'push-0@xs' => 'push-0@xs',
                                                    'push-1@xs' => 'push-1@xs',
                                                    'push-2@xs' => 'push-2@xs',
                                                    'push-3@xs' => 'push-3@xs',
                                                    'push-4@xs' => 'push-4@xs',
                                                    'push-5@xs' => 'push-5@xs',
                                                    'push-6@xs' => 'push-6@xs',
                                                    'push-7@xs' => 'push-7@xs',
                                                    'push-8@xs' => 'push-8@xs',
                                                    'push-9@xs' => 'push-9@xs',
                                                    'push-10@xs' => 'push-10@xs',
                                                    'push-11@xs' => 'push-11@xs',
                                                    'push-12@xs' => 'push-12@xs',
                                                    'push-0@sm' => 'push-0@sm',
                                                    'push-1@sm' => 'push-1@sm',
                                                    'push-2@sm' => 'push-2@sm',
                                                    'push-3@sm' => 'push-3@sm',
                                                    'push-4@sm' => 'push-4@sm',
                                                    'push-5@sm' => 'push-5@sm',
                                                    'push-6@sm' => 'push-6@sm',
                                                    'push-7@sm' => 'push-7@sm',
                                                    'push-8@sm' => 'push-8@sm',
                                                    'push-9@sm' => 'push-9@sm',
                                                    'push-10@sm' => 'push-10@sm',
                                                    'push-11@sm' => 'push-11@sm',
                                                    'push-12@sm' => 'push-12@sm',
                                                    'push-0@md' => 'push-0@md',
                                                    'push-1@md' => 'push-1@md',
                                                    'push-2@md' => 'push-2@md',
                                                    'push-3@md' => 'push-3@md',
                                                    'push-4@md' => 'push-4@md',
                                                    'push-5@md' => 'push-5@md',
                                                    'push-6@md' => 'push-6@md',
                                                    'push-7@md' => 'push-7@md',
                                                    'push-8@md' => 'push-8@md',
                                                    'push-9@md' => 'push-9@md',
                                                    'push-10@md' => 'push-10@md',
                                                    'push-11@md' => 'push-11@md',
                                                    'push-12@md' => 'push-12@md',
                                                    'push-0@lg' => 'push-0@lg',
                                                    'push-1@lg' => 'push-1@lg',
                                                    'push-2@lg' => 'push-2@lg',
                                                    'push-3@lg' => 'push-3@lg',
                                                    'push-4@lg' => 'push-4@lg',
                                                    'push-5@lg' => 'push-5@lg',
                                                    'push-6@lg' => 'push-6@lg',
                                                    'push-7@lg' => 'push-7@lg',
                                                    'push-8@lg' => 'push-8@lg',
                                                    'push-9@lg' => 'push-9@lg',
                                                    'push-10@lg' => 'push-10@lg',
                                                    'push-11@lg' => 'push-11@lg',
                                                    'push-12@lg' => 'push-12@lg',
                                                    'offset-1@xs' => 'offset-1@xs',
                                                    'offset-2@xs' => 'offset-2@xs',
                                                    'offset-3@xs' => 'offset-3@xs',
                                                    'offset-4@xs' => 'offset-4@xs',
                                                    'offset-5@xs' => 'offset-5@xs',
                                                    'offset-6@xs' => 'offset-6@xs',
                                                    'offset-7@xs' => 'offset-7@xs',
                                                    'offset-8@xs' => 'offset-8@xs',
                                                    'offset-9@xs' => 'offset-9@xs',
                                                    'offset-10@xs' => 'offset-10@xs',
                                                    'offset-11@xs' => 'offset-11@xs',
                                                    'offset-12@xs' => 'offset-12@xs',
                                                    'offset-0@sm' => 'offset-0@sm',
                                                    'offset-1@sm' => 'offset-1@sm',
                                                    'offset-2@sm' => 'offset-2@sm',
                                                    'offset-3@sm' => 'offset-3@sm',
                                                    'offset-4@sm' => 'offset-4@sm',
                                                    'offset-5@sm' => 'offset-5@sm',
                                                    'offset-6@sm' => 'offset-6@sm',
                                                    'offset-7@sm' => 'offset-7@sm',
                                                    'offset-8@sm' => 'offset-8@sm',
                                                    'offset-9@sm' => 'offset-9@sm',
                                                    'offset-10@sm' => 'offset-10@sm',
                                                    'offset-11@sm' => 'offset-11@sm',
                                                    'offset-12@sm' => 'offset-12@sm',
                                                    'offset-0@md' => 'offset-0@md',
                                                    'offset-1@md' => 'offset-1@md',
                                                    'offset-2@md' => 'offset-2@md',
                                                    'offset-3@md' => 'offset-3@md',
                                                    'offset-4@md' => 'offset-4@md',
                                                    'offset-5@md' => 'offset-5@md',
                                                    'offset-6@md' => 'offset-6@md',
                                                    'offset-7@md' => 'offset-7@md',
                                                    'offset-8@md' => 'offset-8@md',
                                                    'offset-9@md' => 'offset-9@md',
                                                    'offset-10@md' => 'offset-10@md',
                                                    'offset-11@md' => 'offset-11@md',
                                                    'offset-12@md' => 'offset-12@md',
                                                    'offset-0@lg' => 'offset-0@lg',
                                                    'offset-1@lg' => 'offset-1@lg',
                                                    'offset-2@lg' => 'offset-2@lg',
                                                    'offset-3@lg' => 'offset-3@lg',
                                                    'offset-4@lg' => 'offset-4@lg',
                                                    'offset-5@lg' => 'offset-5@lg',
                                                    'offset-6@lg' => 'offset-6@lg',
                                                    'offset-7@lg' => 'offset-7@lg',
                                                    'offset-8@lg' => 'offset-8@lg',
                                                    'offset-9@lg' => 'offset-9@lg',
                                                    'offset-10@lg' => 'offset-10@lg',
                                                    'offset-11@lg' => 'offset-11@lg',
                                                    'offset-12@lg' => 'offset-12@lg',
                                                ),
                                                'default_value' => array (
                                                    0 => '12@xs',
                                                ),
                                                'allow_null' => 0,
                                                'multiple' => 1,
                                                'ui' => 1,
                                                'ajax' => 1,
                                                'return_format' => 'value',
                                                'placeholder' => '',
                                            ),
                                            array (
                                                'key' => 'field_5808d4d95aa0e',
                                                'label' => 'Column Class',
                                                'name' => 'text_column_class',
                                                'type' => 'text',
                                                'instructions' => '',
                                                'required' => 0,
                                                'conditional_logic' => 0,
                                                'wrapper' => array (
                                                    'width' => '20',
                                                    'class' => '',
                                                    'id' => '',
                                                ),
                                                'default_value' => '',
                                                'placeholder' => '',
                                                'prepend' => '',
                                                'append' => '',
                                                'maxlength' => '',
                                            ),
                                            array (
                                                'key' => 'field_578e4e71b9329',
                                                'label' => 'Content',
                                                'name' => 'wysiwyg_content',
                                                'type' => 'wysiwyg',
                                                'instructions' => '',
                                                'required' => 0,
                                                'conditional_logic' => 0,
                                                'wrapper' => array (
                                                    'width' => '',
                                                    'class' => '',
                                                    'id' => '',
                                                ),
                                                'default_value' => '',
                                                'tabs' => 'all',
                                                'toolbar' => 'full',
                                                'media_upload' => 1,
                                            ),
                                        ),
                                    ),
                                ),
                                'min' => '',
                                'max' => '',
                            ),
                            array (
                                'key' => '57910e3c8741f',
                                'name' => 'tab',
                                'label' => 'Tabbed Row',
                                'display' => 'block',
                                'sub_fields' => array (
                                    array (
                                        'key' => 'field_57910e3c87420',
                                        'label' => 'Row Header',
                                        'name' => '',
                                        'type' => 'message',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => 0,
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'message' => '',
                                        'new_lines' => 'wpautop',
                                        'esc_html' => 0,
                                    ),
                                    array (
                                        'key' => 'field_57910e3c87421',
                                        'label' => 'Toggle Header',
                                        'name' => 'toggle_header',
                                        'type' => 'true_false',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => 0,
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => 'toggler',
                                            'id' => '',
                                        ),
                                        'message' => 'Toggle Row Header',
                                        'default_value' => 0,
                                    ),
                                    array (
                                        'key' => 'field_57910e3c87422',
                                        'label' => 'Pre-Title',
                                        'name' => 'text_pretitle',
                                        'type' => 'text',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_57910e3c87421',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 25,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'default_value' => '',
                                        'placeholder' => 'If necessary',
                                        'prepend' => '',
                                        'append' => '',
                                        'maxlength' => '',
                                        'readonly' => 0,
                                        'disabled' => 0,
                                    ),
                                    array (
                                        'key' => 'field_57910e3c87423',
                                        'label' => 'Title',
                                        'name' => 'text_title',
                                        'type' => 'text',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_57910e3c87421',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 35,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'default_value' => '',
                                        'placeholder' => 'If necessary',
                                        'prepend' => '',
                                        'append' => '',
                                        'maxlength' => '',
                                        'readonly' => 0,
                                        'disabled' => 0,
                                    ),
                                    array (
                                        'key' => 'field_57910e3c87424',
                                        'label' => 'Sub Title',
                                        'name' => 'text_subtitle',
                                        'type' => 'textarea',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_57910e3c87421',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 40,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'default_value' => '',
                                        'placeholder' => '',
                                        'maxlength' => '',
                                        'rows' => 1,
                                        'new_lines' => 'wpautop',
                                        'readonly' => 0,
                                        'disabled' => 0,
                                    ),
                                    array (
                                        'key' => 'field_57910e3c87425',
                                        'label' => 'Row Settings',
                                        'name' => '',
                                        'type' => 'message',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => 0,
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'message' => '',
                                        'new_lines' => 'wpautop',
                                        'esc_html' => 0,
                                    ),
                                    array (
                                        'key' => 'field_57910e3c87426',
                                        'label' => 'Toggle Settings',
                                        'name' => 'toggle_settings',
                                        'type' => 'true_false',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => 0,
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => 'toggler',
                                            'id' => '',
                                        ),
                                        'message' => 'Toggle Row Settings',
                                        'default_value' => 0,
                                    ),
                                    array (
                                        'key' => 'field_57910e3c87427',
                                        'label' => 'Container',
                                        'name' => 'select_container',
                                        'type' => 'select',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_57910e3c87426',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 20,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'choices' => array (
                                            'layout__container--basic' => 'Basic',
                                            'layout__container--fluid' => 'Fluid',
                                            'layout__container--none' => 'None',
                                        ),
                                        'default_value' => array (
                                        ),
                                        'allow_null' => 0,
                                        'multiple' => 0,
                                        'ui' => 0,
                                        'ajax' => 0,
                                        'placeholder' => '',
                                        'disabled' => 0,
                                        'readonly' => 0,
                                        'return_format' => 'value',
                                    ),
                                    array (
                                        'key' => 'field_57910e3c87428',
                                        'label' => 'ID',
                                        'name' => 'text_id',
                                        'type' => 'text',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_57910e3c87426',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 20,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'default_value' => '',
                                        'placeholder' => 'lowercase-class',
                                        'prepend' => '',
                                        'append' => '',
                                        'maxlength' => '',
                                        'readonly' => 0,
                                        'disabled' => 0,
                                    ),
                                    array (
                                        'key' => 'field_57910e3c87429',
                                        'label' => 'Class',
                                        'name' => 'text_class',
                                        'type' => 'text',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_57910e3c87426',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 20,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'default_value' => '',
                                        'placeholder' => 'lowercase-class',
                                        'prepend' => '',
                                        'append' => '',
                                        'maxlength' => '',
                                        'readonly' => 0,
                                        'disabled' => 0,
                                    ),
                                    array (
                                        'key' => 'field_57910e3c8742b',
                                        'label' => 'Background Color',
                                        'name' => 'select_background',
                                        'type' => 'select',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_57910e3c87426',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 20,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'choices' => array (
                                            'Default' => 'Default',
                                            'Dark' => 'Dark',
                                            'Light' => 'Light',
                                            'Primary' => 'Primary',
                                            'Secondary' => 'Secondary',
                                            'Tertiary' => 'Tertiary',
                                        ),
                                        'default_value' => array (
                                            0 => 'Default',
                                        ),
                                        'allow_null' => 0,
                                        'multiple' => 0,
                                        'ui' => 0,
                                        'ajax' => 0,
                                        'placeholder' => '',
                                        'disabled' => 0,
                                        'readonly' => 0,
                                        'return_format' => 'value',
                                    ),
                                    array (
                                        'key' => 'field_57910e3c8742c',
                                        'label' => 'Background Image',
                                        'name' => 'image_background',
                                        'type' => 'image',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_57910e3c87426',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 20,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'return_format' => 'array',
                                        'preview_size' => 'thumbnail',
                                        'library' => 'all',
                                        'min_width' => '',
                                        'min_height' => '',
                                        'min_size' => '',
                                        'max_width' => '',
                                        'max_height' => '',
                                        'max_size' => '',
                                        'mime_types' => '',
                                    ),
                                    array (
                                        'key' => 'field_57910e3c8742d',
                                        'label' => 'Row Content',
                                        'name' => '',
                                        'type' => 'message',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => 0,
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'message' => '',
                                        'new_lines' => 'wpautop',
                                        'esc_html' => 0,
                                    ),
                                    array (
                                        'key' => 'field_57910e3c8742e',
                                        'label' => 'Toggle Row Content',
                                        'name' => 'toggle_content',
                                        'type' => 'true_false',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => 0,
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => 'toggler',
                                            'id' => '',
                                        ),
                                        'message' => 'Toggle Row Content',
                                        'default_value' => 1,
                                    ),
                                    array (
                                        'key' => 'field_57910e3c8742f',
                                        'label' => 'Tabs',
                                        'name' => 'repeater_tabs',
                                        'type' => 'repeater',
                                        'instructions' => 'Add tabbed content to the row.',
                                        'required' => 0,
                                        'conditional_logic' => 0,
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'collapsed' => '',
                                        'min' => 2,
                                        'max' => '',
                                        'layout' => 'table',
                                        'button_label' => 'Add Tab',
                                        'sub_fields' => array (
                                            array (
                                                'key' => 'field_57910e3c87430',
                                                'label' => 'Title',
                                                'name' => 'text_title',
                                                'type' => 'text',
                                                'instructions' => '',
                                                'required' => 0,
                                                'conditional_logic' => 0,
                                                'wrapper' => array (
                                                    'width' => 20,
                                                    'class' => '',
                                                    'id' => '',
                                                ),
                                                'default_value' => '',
                                                'placeholder' => '',
                                                'prepend' => '',
                                                'append' => '',
                                                'maxlength' => '',
                                                'readonly' => 0,
                                                'disabled' => 0,
                                            ),
                                            array (
                                                'key' => 'field_57910e3c87431',
                                                'label' => 'Content',
                                                'name' => 'wysiwyg_content',
                                                'type' => 'wysiwyg',
                                                'instructions' => '',
                                                'required' => 0,
                                                'conditional_logic' => 0,
                                                'wrapper' => array (
                                                    'width' => '',
                                                    'class' => '',
                                                    'id' => '',
                                                ),
                                                'default_value' => '',
                                                'tabs' => 'all',
                                                'toolbar' => 'full',
                                                'media_upload' => 1,
                                            ),
                                        ),
                                    ),
                                ),
                                'min' => '',
                                'max' => '',
                            ),
                            array (
                                'key' => '57923dcabeb60',
                                'name' => 'accordion',
                                'label' => 'Accordion Row',
                                'display' => 'block',
                                'sub_fields' => array (
                                    array (
                                        'key' => 'field_57923dcabeb61',
                                        'label' => 'Row Header',
                                        'name' => '',
                                        'type' => 'message',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => 0,
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'message' => '',
                                        'new_lines' => 'wpautop',
                                        'esc_html' => 0,
                                    ),
                                    array (
                                        'key' => 'field_57923dcabeb62',
                                        'label' => 'Toggle Header',
                                        'name' => 'toggle_header',
                                        'type' => 'true_false',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => 0,
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => 'toggler',
                                            'id' => '',
                                        ),
                                        'message' => 'Toggle Row Header',
                                        'default_value' => 0,
                                    ),
                                    array (
                                        'key' => 'field_57923dcabeb63',
                                        'label' => 'Pre-Title',
                                        'name' => 'text_pretitle',
                                        'type' => 'text',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_57923dcabeb62',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 25,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'default_value' => '',
                                        'placeholder' => 'If necessary',
                                        'prepend' => '',
                                        'append' => '',
                                        'maxlength' => '',
                                        'readonly' => 0,
                                        'disabled' => 0,
                                    ),
                                    array (
                                        'key' => 'field_57923dcabeb64',
                                        'label' => 'Title',
                                        'name' => 'text_title',
                                        'type' => 'text',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_57923dcabeb62',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 35,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'default_value' => '',
                                        'placeholder' => 'If necessary',
                                        'prepend' => '',
                                        'append' => '',
                                        'maxlength' => '',
                                        'readonly' => 0,
                                        'disabled' => 0,
                                    ),
                                    array (
                                        'key' => 'field_57923dcabeb65',
                                        'label' => 'Sub Title',
                                        'name' => 'text_subtitle',
                                        'type' => 'textarea',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_57923dcabeb62',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 40,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'default_value' => '',
                                        'placeholder' => '',
                                        'maxlength' => '',
                                        'rows' => 1,
                                        'new_lines' => 'wpautop',
                                        'readonly' => 0,
                                        'disabled' => 0,
                                    ),
                                    array (
                                        'key' => 'field_57923dcabeb66',
                                        'label' => 'Row Settings',
                                        'name' => '',
                                        'type' => 'message',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => 0,
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'message' => '',
                                        'new_lines' => 'wpautop',
                                        'esc_html' => 0,
                                    ),
                                    array (
                                        'key' => 'field_57923dcabeb67',
                                        'label' => 'Toggle Settings',
                                        'name' => 'toggle_settings',
                                        'type' => 'true_false',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => 0,
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => 'toggler',
                                            'id' => '',
                                        ),
                                        'message' => 'Toggle Row Settings',
                                        'default_value' => 0,
                                    ),
                                    array (
                                        'key' => 'field_57923dcabeb68',
                                        'label' => 'Container',
                                        'name' => 'select_container',
                                        'type' => 'select',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_57923dcabeb67',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 20,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'choices' => array (
                                            'layout__container--basic' => 'Basic',
                                            'layout__container--fluid' => 'Fluid',
                                            'layout__container--none' => 'None',
                                        ),
                                        'default_value' => array (
                                        ),
                                        'allow_null' => 0,
                                        'multiple' => 0,
                                        'ui' => 0,
                                        'ajax' => 0,
                                        'placeholder' => '',
                                        'disabled' => 0,
                                        'readonly' => 0,
                                        'return_format' => 'value',
                                    ),
                                    array (
                                        'key' => 'field_57923dcabeb69',
                                        'label' => 'ID',
                                        'name' => 'text_id',
                                        'type' => 'text',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_57923dcabeb67',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 20,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'default_value' => '',
                                        'placeholder' => 'lowercase-class',
                                        'prepend' => '',
                                        'append' => '',
                                        'maxlength' => '',
                                        'readonly' => 0,
                                        'disabled' => 0,
                                    ),
                                    array (
                                        'key' => 'field_57923dcabeb6a',
                                        'label' => 'Class',
                                        'name' => 'text_class',
                                        'type' => 'text',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_57923dcabeb67',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 20,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'default_value' => '',
                                        'placeholder' => 'lowercase-class',
                                        'prepend' => '',
                                        'append' => '',
                                        'maxlength' => '',
                                        'readonly' => 0,
                                        'disabled' => 0,
                                    ),
                                    array (
                                        'key' => 'field_57923dcabeb6c',
                                        'label' => 'Background Color',
                                        'name' => 'select_background',
                                        'type' => 'select',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_57923dcabeb67',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 20,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'choices' => array (
                                            'Default' => 'Default',
                                            'Dark' => 'Dark',
                                            'Light' => 'Light',
                                            'Primary' => 'Primary',
                                            'Secondary' => 'Secondary',
                                            'Tertiary' => 'Tertiary',
                                        ),
                                        'default_value' => array (
                                            0 => 'Default',
                                        ),
                                        'allow_null' => 0,
                                        'multiple' => 0,
                                        'ui' => 0,
                                        'ajax' => 0,
                                        'placeholder' => '',
                                        'disabled' => 0,
                                        'readonly' => 0,
                                        'return_format' => 'value',
                                    ),
                                    array (
                                        'key' => 'field_57923dcabeb6d',
                                        'label' => 'Background Image',
                                        'name' => 'image_background',
                                        'type' => 'image',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => array (
                                            array (
                                                array (
                                                    'field' => 'field_57923dcabeb67',
                                                    'operator' => '==',
                                                    'value' => '1',
                                                ),
                                            ),
                                        ),
                                        'wrapper' => array (
                                            'width' => 20,
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'return_format' => 'array',
                                        'preview_size' => 'thumbnail',
                                        'library' => 'all',
                                        'min_width' => '',
                                        'min_height' => '',
                                        'min_size' => '',
                                        'max_width' => '',
                                        'max_height' => '',
                                        'max_size' => '',
                                        'mime_types' => '',
                                    ),
                                    array (
                                        'key' => 'field_57923dcabeb6e',
                                        'label' => 'Row Content',
                                        'name' => '',
                                        'type' => 'message',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => 0,
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'message' => '',
                                        'new_lines' => 'wpautop',
                                        'esc_html' => 0,
                                    ),
                                    array (
                                        'key' => 'field_57923dcabeb6f',
                                        'label' => 'Accordion Row Content',
                                        'name' => 'accordion_content',
                                        'type' => 'true_false',
                                        'instructions' => '',
                                        'required' => 0,
                                        'conditional_logic' => 0,
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => 'toggler',
                                            'id' => '',
                                        ),
                                        'message' => 'Toggle Row Content',
                                        'default_value' => 1,
                                    ),
                                    array (
                                        'key' => 'field_57923dcabeb70',
                                        'label' => 'Accordion',
                                        'name' => 'repeater_accordion',
                                        'type' => 'repeater',
                                        'instructions' => 'Add accordion content to the row.',
                                        'required' => 0,
                                        'conditional_logic' => 0,
                                        'wrapper' => array (
                                            'width' => '',
                                            'class' => '',
                                            'id' => '',
                                        ),
                                        'collapsed' => '',
                                        'min' => 1,
                                        'max' => '',
                                        'layout' => 'table',
                                        'button_label' => 'Add Accordion',
                                        'sub_fields' => array (
                                            array (
                                                'key' => 'field_57923dcabeb71',
                                                'label' => 'Title',
                                                'name' => 'text_title',
                                                'type' => 'text',
                                                'instructions' => '',
                                                'required' => 0,
                                                'conditional_logic' => 0,
                                                'wrapper' => array (
                                                    'width' => 20,
                                                    'class' => '',
                                                    'id' => '',
                                                ),
                                                'default_value' => '',
                                                'placeholder' => '',
                                                'prepend' => '',
                                                'append' => '',
                                                'maxlength' => '',
                                                'readonly' => 0,
                                                'disabled' => 0,
                                            ),
                                            array (
                                                'key' => 'field_57923dcabeb72',
                                                'label' => 'Content',
                                                'name' => 'wysiwyg_content',
                                                'type' => 'wysiwyg',
                                                'instructions' => '',
                                                'required' => 0,
                                                'conditional_logic' => 0,
                                                'wrapper' => array (
                                                    'width' => '',
                                                    'class' => '',
                                                    'id' => '',
                                                ),
                                                'default_value' => '',
                                                'tabs' => 'all',
                                                'toolbar' => 'full',
                                                'media_upload' => 1,
                                            ),
                                        ),
                                    ),
                                ),
                                'min' => '',
                                'max' => '',
                            ),
                        ),
                    ),
                ),
                'location' => array (
                    array (
                        array (
                            'param' => 'post_type',
                            'operator' => '==',
                            'value' => 'page',
                        ),
                    ),
                    array (
                        array (
                            'param' => 'options_page',
                            'operator' => '==',
                            'value' => 'acf-options-homepage',
                        ),
                    ),
                ),
                'menu_order' => 0,
                'position' => 'normal',
                'style' => 'seamless',
                'label_placement' => 'top',
                'instruction_placement' => 'label',
                'hide_on_screen' => array (
                    0 => 'the_content',
                ),
                'active' => 1,
                'description' => '',
            ));
        }
    }
}

new Page;
