<?php
/**
 * Plugin Name: PODS Layout
 * Description: WP Plugin to create layout options for page headers, body and navigation
 * Version: 1.0.3
 * Author: PODS
 * Text Domain: pods
 */

namespace PODS\Layout;


// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

require_once(dirname( __FILE__ ) . '/pods-layout-header.php');
require_once(dirname( __FILE__ ) . '/pods-layout-page.php');
require_once(dirname( __FILE__ ) . '/pods-layout-navigation.php');
